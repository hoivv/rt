import React, {Component} from 'react';

class Api extends Component{
    state = {
        data: []
    };
    
    async componentDidMount(){
        const url =
        "https://api.covid19api.com/countries"
        // "https://en.wikipedia.org/w/api.php?action=opensearch&search=Seona+Dancing&format=json&origin=*";

        fetch(url)
        .then((result) => result.json())
        .then((result) => {
            this.setState({
                data: result
            });
            // this.state = {
            //     data: result
            // };
        });
    }

    render(){
        const {data} = this.state;

        const result = data.map((entry, index) => {
        return <li key={index}>{entry.Country}</li>
        });

        return <ol>{result}</ol>
    }
}

export default Api;