import React from 'react';

const Table = (props) => {
    const { characterData, removeCharacter } = props;

    return (
        <table>
            <TableHeader />
            <TableBody characterData={characterData} removeCharacter={removeCharacter} />
        </table>
    );
}

//arrow func -> component
const TableHeader = () => {
    return (
        <thead>
            <tr>
                <th>Name</th>
                <th>Job</th>
                <th>Remove</th>
            </tr>
        </thead>
    );
};

const TableBody = (props) => {
    const rows = props.characterData.map((row, index) => {
        return (
            <tr key={index}>
                <td>{row.name}</td>
                <td>{row.job}</td>
                <td>
                    <button style={{color: "#fff", backgroundColor: "#8b0000", border: "none"}} onClick={() => props.removeCharacter(index)}>DELETE</button>
                </td>
            </tr>
        );
    })

    return rows.length > 0 ? <tbody>{rows}</tbody> : <caption>Nothing here now.</caption>;
};

export default Table;