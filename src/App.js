import React, {Component} from 'react';
import Table from './Table';
import Form from './Form';

import Api from './Api';

class App extends Component {
  state = {
    characters: [
      // {
      //   name: "Charlie",
      //   job: "Janitor"
      // },
      // {
      //   name: 'Mac',
      //   job: 'Bouncer',
      // },
      // {
      //   name: 'Dee',
      //   job: 'Aspring actress',
      // },
      // {
      //   name: 'Dennis',
      //   job: 'Bartender',
      // }
    ]
  };

  removeCharacter = (index) => {
    const { characters } = this.state; //destructure

    this.setState({
      characters: characters.filter((character, i) => {
        return i !== index;
      })
    });
  }

  handleSubmit = (character) => {
    const {characters} = this.state;

    if(character.name !== "" && character !== ""){
      this.setState({characters: [...characters, character]});
      document.querySelector("#msg").innerHTML = "";
    }
    else
      document.querySelector("#msg").innerHTML = "Please fill input(s).";
  }

  render() {
    const { characters } = this.state;

    return (
      <div className="container">
        <h1>RT</h1>
        <h2>List table</h2>
        <Table characterData={characters} removeCharacter={this.removeCharacter} />
        <h2>Add new</h2>
        <span id="msg" style={{color: "#ff0000"}}></span>
        <Form handleSubmit={this.handleSubmit}/>
        
        <h2>API</h2>
        <Api/>
      </div>
    );
  }
}

export default App;